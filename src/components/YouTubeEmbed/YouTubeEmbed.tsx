import React from 'react'

interface YouTubeEmbedProps {
    id: string,
    width?: number,
    height?: number
}

const YouTubeEmbed: React.FC<YouTubeEmbedProps> = (props) => {
    return (
        <iframe style={{marginTop: 5, marginBottom: 5, border: '2px solid black', maxWidth: '100%'}}
            width={props?.width?? 720} 
            height={props?.height?? 405} 
            src={`https://www.youtube.com/embed/${props.id}`}
            allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" 
            allowFullScreen>
        </iframe>
    )
}

export default YouTubeEmbed
