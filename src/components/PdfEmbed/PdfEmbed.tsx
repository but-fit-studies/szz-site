import React from 'react'

interface PdfEmbedProps {
    fileId: string
}

const PdfEmbed: React.FC<PdfEmbedProps> = (props) => {
    return (
        <iframe style={{marginTop: 5, marginBottom: 5, border: '2px solid black'}} 
            width="100%" 
            height={500} 
            src={`/pdf/${props.fileId}`}
            allowFullScreen>
        </iframe>
    )
}

export default PdfEmbed
