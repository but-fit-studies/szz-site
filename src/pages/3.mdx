---
layout: ../layouts/MDXPage.astro
currentPage: Sekvenční logické obvody
---

import YouTubeEmbed from './../components/YouTubeEmbed/YouTubeEmbed.tsx'
import PdfEmbed from './../components/PdfEmbed/PdfEmbed.tsx'

# 3.: Sekvenční logické obvody

- ### Klopné obvody
- ### Čítače
- ### Registry
- ### Stavové automaty – Reprezentace a implementace


## Obecný princip

U sekvenčních logických obvodů výstup závisí nejen na vstupu (jako je tomu u kombinačních logických obvodů), ale také na předchozích vstupech -- obvod má "paměť". V této paměti je uchován současný stav obvodu, který posléze spolu se vstupy určuje následující stav a výstupy.

### Synchronní a asynchronní sekvenční logické obvody

Sekvenční logické obvody se dělí na **synchronní** a **asynchronní**.

#### Synchronní sekvenční logické obvody

Jsou synchronizovány (řízeny) tzv. hodinovým signálem.
Změny stavu tak probíhají periodicky.
To zajišťuje předvídatelné chování a umožňuje snazší konstrukci složitějšího hardware.
Oproti asynchronním sekvenčním logickým obvodům jsou ovšem pomalejší a jejich konstrukce náročnější (dražší).
**Synchronní** sekvenční logické **obvody** **se** dále **dělí** **na** **hranové** a **úrovňové**.

##### Hranové synchronní sekvenční logické obvody

Reagují na změnu logické úrovně hodinového signálu -- z 0 na 1 (náběžná hrana) či 1 na 0 (sestupná hrana).
To je činí přesnějšími.

##### Úrovňové synchronní sekvenční logické obvody

Reagují na konkrétní logickou úroveň hodinového signálu -- 0 či 1.
Změny tak mohou nastat v širším časovém rámci, což činí tyto obvody náchylnějšími k problémům.

#### Asynchronní sekvenční logické obvody

Nečekají na hodinový signál, což je činí rychlejšími, ale také náchylnějšími k problémům, jak jsou tzv. hazardy.
Změna stavu probíhá okamžitě, v závislosti na změně vstupních signálů.

## Klopné obvody

Klopné obvody jsou základními sekvenčními logickými obvody, které skokově přechází mezi několika diskrétními stavy.
Lze je považovat za nejjednodušší formu digitální paměti -- umožňují uchovat jednobitovou informaci.
Představují stavební bloky, pomocí kterých konstruujeme složitější sekvenční logické obvody, jako jsou čítače, registry, atd.
Speciálními typy klopných obvodů jsou **astabilní**, **monostabilní** a **Schmittovy** klopné obvody.

- **Astabilní klopné obvody** -- Nemají žádný stabilní stav a neustále tak oscilují mezi dvěma stavy.
Využívají se při generování hodinových signálů.

- **Monostabilní klopné obvody** -- Mají jeden stabilní stav, který při aktivaci vstupním signálem dočasně opustí, ale po určitém
čase se do něj opět navrátí. Používají se při generování časových impulzů a nebo pro zpoždění signálu.

- **Schmittovy klopné obvody** -- Mají dvě prahové hodnoty: jednu pro aktivaci (přechod z 0 do 1) a druhou pro deaktivaci (přechod z 1 do 0). Rozdíl v prahových hodnotách, označovaný jako **hystereze**, zabraňuje nechtěným změnám stavu a výstupu na základě malých, nevýznamných změn vstupu (šum).
Používá se k eliminaci šumu a to např. při převodu analogových signálů na digitální, či při stabilizaci signálu z mechanických spínačů (debouncing).

Nejpoužívanějším typem klopných obvodů jsou **bistabilní klopné obvody**, označované také jako **flip-flop**, které mají dva stabilní stavy.

### Typy bistabilních klopných obvodů

#### SET

Primitivní, prakticky těžko použitelný obvod, zkonstruovaný pomocí hradla AND, prvku zajišťujícího zpoždění a zpětné vazby. **Umožňuje změnu stavu pouze jedním směrem (z 0 na 1)**. Tento obvod ilustruje diagram a Moorův automat níže.:

<img src="/images/3/1.png" alt="" width="800"/>

#### R-S (Reset-Set)

Přivedení logickou 1 na vstup S (Set) změní stav a tedy i výstup Q na 1.
Přivedeme-li logickou 1 na vstup R (Reset), změní se stav a tedy i výstup Q na 0.
Přivedeme-li na oba vstupy logickou 0, bude stav zůstane beze změny a výstup Q bude odpovídat tomuto předchozímu stavu.
Přivedení logické 1 na oba vstupy -- S i R je zakázaná kombinace.

<img src="/images/3/2.png" alt="" width="600"/>

<img src="/images/3/3.png" alt="" width="300"/>

#### R-S (Reset-Set) s povolovacím vstupem

Oproti klasickému R-S má navíc povolovací vstup C, přičemž obvod lze nastavit (Set) či nulovat (Reset) pouze pokud je na vstup C přivedena logická 1.

#### J-K (s povolovacím vstupem)

Obvod J-K je obdobou obvodu R-S (K: R; J: S). Liší se zavedením zpětné vazby, čímž je eliminována zakázaná kombinace.
Je-li na oba vstupy obvodu J-K přivedena logická 1, dochází k "překlopení" (toggle) aktuálního stavu a tedy i hodnoty na výstupu.

<img src="/images/3/4.png" alt="" width="800"/>

#### T (Toggle) (s povolovacím vstupem)

Jedná se o variantu obvodu J-K, kde na oba vstupy posíláme jednu hodnotu -- T.
Pokud je touto hodnotou logická 0, stav i výstup zůstává beze změny.
Je-li touto hodnotou logická 1, dochází k překlopení stavu a tedy i výstupu.
Držíme-li na vstupu T dlouhodobě logickou 1, dochází k oscilaci stavu a tedy i výstupu mezi logickou 0 a 1.

<img src="/images/3/5.png" alt="" width="800"/>

#### D (Data / Delay)

Tento obvod založený na obvodu R-S, přičemž na vstup S posíláme hodnotu D a vstup R negaci hodnoty D.
Díky tomu nemůže dojít k zakázané kombinaci (logická 1 na obou vstupech) a na výstupu Q je vždy hodnota D.
Účelem tohoto obvodu je tvorba zpoždění.
Obvod je základním stavebním prvkem složitějších paměťových obvodů.

<img src="/images/3/6.png" alt="" width="400"/>

#### D (Data / Delay) s povolovacím vstupem

Od klasického obvodu D (Data / Delay) se liší v tom, že změna stavu, potažmo hodnoty na výstupu je možná
pouze pokud je na povolovací vstup C přivedena logická 1. V případě, že je na tento vstup přivedena logická 0, obvod "drží" poslední hodnotu a na změny hodnoty na vstupu D nereaguje.

<img src="/images/3/7.png" alt="" width="800"/>

### 2-fázové klopné obvody

Mají 2 oddělené fáze. V první fázi jsou data nahrána do obvodu. V druhé fázi pak dojde k jejich "uložení" a přivedení na výstup.
To zvyšuje odolnost obvodu proti šumu a přechodovým jevům.
Typickou realizaci těchto obvodů představuje konfigurace master-slave, tvořená dvojicí identických klopných obvodů, z nichž každý reaguje na jinou úroveň potažmo hranu hodinového signálu a realizuje jednu fázi.

<img src="/images/3/8a.png" alt="" width="600"/>

U 2-fázových obvodů se znázorňuje reakce na určitý pulz hodinového signálu -- sekvenci 2 odlišných logických úrovní.
Kladný pulz (0-1) značíme symbolem ˩ a záporný pulz (1-0) symbolem ˥. Tyto symboly se typicky objevují u výstupů, viz obrázek níže.:

<img src="/images/3/8b.png" alt="" width="200"/>

### Derivační (aka hranové) klopné obvody

Reagují na hranu hodinového signálu (Edge-triggered flip-flops)
O tom, jestli se jedná o náběžnou či sestupnou hranu rozhoduje symbol u CLK: symbol > značí reakci na náběžnou hranu, zatím co symbol < reakci na sestupnou hranu.

<img src="/images/3/9.png" alt="" width="200"/>

## Čítače

### Synchronní čítače

- Speciální případ synchronního automatu -- reaguje na hodinový signál a přechází mezi stavy v rámci čítací posloupnosti.
- Některé čítače mohou na základě logické hodnoty přivedená na vstup (označovaný např. **UP/DOWN**) měnit **"směr" čítání**.
- Čítače mají také **výstup pro indikaci přetečení -- RCO (Ripple Carry Output), TC (Terminal Count)** atp.
Přetečení je situace, kdy čítač dojde na konec posloupnosti stavů a nemůže pokračovat, např. přechod z hodnoty (stavu) 15 na hodnotu (stav) 0 u čtyřbitového čítače čítajícího vzestupně (nahoru).
- Většinou mají také **povolovací vstup -- CE (Clock Enable), CTEN (Counter Clock Enable)**, atp. 
Logická 0 přivedená na tento vstup způsobí, že si čítač bude "pamatovat" poslední hodnotu (setrvá v momentálním stavu).
Logická 1 je pak předpokladem pro fungování čítače ve "standardním" režimu -- přechod mezi stavy v dané posloupnost v reakci na hodinový signál.
- Prodloužení čítací posloupnosti, resp. dělícího poměru (**čítače se často používají pro dělení frekvence / realizace operátoru modulo**) dosáhneme **zapojením vícero čítačů do kaskády**, přičemž napojíme výstup RCO (popř. TC) předchozího čítače na povolovací vstup (CE) následujícího čítače.

#### Realizace 4-bitového čítače pomocí derivačního (hranového) bistabilního klopného obvodu T (Toggle).

<img src="/images/3/10.png" alt="" width="600"/>

- K přetečení (logická 1 na výstupu RCO) dochází při přechodu z 16. stavu (15 (1111)) do 1. stavu (0 (0000)).
- Aktuální hodnotu čítače získáme z výstupů Q<sub>0</sub> až Q<sub>3</sub>.
- Kaskáda -- obdoba šíření přenosu v kaskádní sčítačce (RCA) -- vyžaduje delší čas pro ustálení hodnoty

#### Alternativní, "paralelní" realizace

- Rychlejší, ale také složitější než předchozí, kaskádní realizace

<img src="/images/3/11.png" alt="" width="600"/>

### "Speciální" čítače

### Čítač one-hot (Straight ring counter)

- Zkonstruován pomocí klopných obvodů D (Data / Delay)
- Logickou 1 drží pouze 1 obvod
- Výstup posledního obvodu je přiveden na vstup prvního (ring -- kruh)
- Počet stavů (délka čítací posloupnosti) je rovna počtu obvodů D
- Při inicializaci čítače jsou všechny obvody D s výjimkou jednoho inicializovány do stavu 0

Obrázek níže zobrazuje 3-bitový one-hot čítač.:

<img src="/images/3/12.png" alt="" width="600"/>

a dále tabulka zobrazující stav a hodnoty výstupů Q<sub>n</sub> 2-bitové varianty tohoto čítače.:

<img src="/images/3/13.png" alt="" width="150"/>

### Johnsonův čítač (Johnson counter; Twisted ring counter)

- Na vstup prvního obvodu D je přiveden **negovaný** výstup posledního
- Výhodou je, že počet stavů čítače (délka čítací posloupnosti) je rovna dvojnásobku počtu obvodů D

Tabulka zobrazující stav a hodnoty výstupů Q<sub>n</sub> 3-bitové varianty tohoto čítače.:

<img src="/images/3/14.png" alt="" width="150"/>

## Registry

Registry slouží jako paměťové prvky složitějších digitálních obvodů.
Konstruují se nejčastěji pomocí bistabilních klopných obvodů D (Data / Delay).

### Posuvné registry

Existují čtyři základní konfigurace posuvných registrů.:

- Serial-In Serial-Out (SISO)

<img src="/images/3/15.png" alt="" width="400"/>

- Serial-In Parallel-Out (SIPO)

<img src="/images/3/16.png" alt="" width="300"/>

- Parallel-In Serial-Out (PISO)

<img src="/images/3/17.png" alt="" width="300"/>

- Parallel-In Parallel-Out (PIPO)

<img src="/images/3/18.png" alt="" width="200"/>

- Nad rámec těchto základních konfigurací je potřeba řešit určité nuance, např. směr posunu.

- Obrázek níže zobrazuje detailní schéma 8-bitového posuvného registru v konfiguraci PIPO (Parallel-In Parallel-Out) a také schématickou značku používanou v rámci složitějších schémat "vyšší úrovně".

<img src="/images/3/19.png" alt="" width="800"/>

- U posuvných registrů běžně narazíme na povolovací vstup, synchronizaci pomocí hodinového signálu a také vstup pro
asynchronní reset (nulování).

## Konečné stavové automaty (FSM)

V kontextu digitální obvodů (a předmětu INC, ze kterého vychází náplň tohoto okruhu) pojmem konečné stavové automaty
myslíme jejich speciální typ označovaný jako konečné stavové převodníky, z angl. Finite-State Transducers (FSTs).
Ty se od klasických stavových automatů (Finite State Acceptors (FSAs)) liší tím, že umožňuje generování výstupu.
Tvoří je šestice (Q, Σ, Γ, δ, λ, q₀), kde Q je množina stavů, Σ (Sigma) je vstupní abeceda, Γ (Gamma) je výstupní abeceda, δ (Delta) je 
přechodová funkce (pravidla pro přechod mezi stavy) (**δ: Q × Σ → Q**), λ (Lambda) je výstupní funkce a q₀ je počáteční stav (náležící do množiny stavů Q).

Konečné stavové převodníky se dále dělí na 2 typy, které se liší v tom, jak generují výstup (tedy z hlediska výstupní funkce).:

- Moorovy stroje (či automaty)
  - Výstupní funkce: **λ : Q → Γ**
  - **Výstup je funkcí aktuálního stavu** a **změna na výstupu se** tak projeví až **po přechodu do následujícího stavu** -- zpozdění
  - Výstup nalezneme v rámci označení stavu -- stav je označen stylem **jméno stavu/výstup**
    - U přechodu pak nalezneme pouze označení vstupu

<img src="/images/3/21.png" alt="" width="400"/>

- Mealyho stroje (či automaty)
  - Výstupní funkce: **λ : Q × Σ → Γ**
  - **Výstup je funkcí aktuálního stavu a vstupu**
  - **Změna na výstupu se projeví při přechodu do následujícího stavu**
  - Výstup nalezneme v rámci označení přechodu -- přechod je označen stylem **vstup/výstup**

<img src="/images/3/20.png" alt="" width="300"/>

## Implementace

Implementaci konečných stavových automatů (resp. konečných stavových převodníků) v digitální elektronice ilustruje následující schéma.:
Přechodová funkce δ (určující následující stav na základě vstupu a současného stavu) je realizována pomocí kombinační logické sítě.
Kombinační logické sítě jsou též využity pro výstupy obvodu. Jde-li o Moorův stroj, je na vstup této kombinační logiky přiveden pouze
současný stav. Jde-li o Mealyho stroj, je potřeba také vstup. Důležité je též uložení a průběžná aktualizace stavu. K tomu je potřeba paměť,
která představuje sekvenční obvod -- typicky registr.

<img src="/images/3/22.png" alt="" width="800"/>

## Doplňující materiály

### FIT

#### Přednášky z předmětu INC

<PdfEmbed fileId="3/1.pdf" />

<PdfEmbed fileId="3/2.pdf" />

#### Stream (leSamo)

<YouTubeEmbed id="wCLyuxKHx4g" />

### Další zdroje

#### YouTube -- Synchronní UP/DOWN čítač

<YouTubeEmbed id="svFUEJkoeVY" />

#### YouTube -- Moore Machine to Mealy Machine

<YouTubeEmbed id="HEVWx4irOx4" />

#### YouTube -- Mealy Machine to Moore Machine

<YouTubeEmbed id="-etILQcfgTg" />

#### YouTube -- From a Finite State Machine to a Circuit

<YouTubeEmbed id="Z4Zz7n-Lj0g" />
