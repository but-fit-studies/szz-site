---
layout: ../layouts/MDXPage.astro
currentPage: Kombinační logické obvody
---

import YouTubeEmbed from './../components/YouTubeEmbed/YouTubeEmbed.tsx'
import PdfEmbed from './../components/PdfEmbed/PdfEmbed.tsx'

# 2.: Kombinační logické obvody

- ### Multiplexor
- ### Demultiplexor
- ### Kodér
- ### Dekodér
- ### Binární sčítačka

## Obecný princip

Struktura kombinačních obvodů neobsahuje cykly resp. zpětné vazby.
Obvod tedy nemá "paměť" -- **hodnoty na výstupu závisí pouze na kombinaci hodnot na vstupu**.

## Reprezentace

Kombinační logické obvody (resp. kombinační logické sítě) můžeme reprezentovat následujícími způsoby.:

- Výrazy Booleovy algebry
- Pravdivostní tabulky
- Karnaughovy mapy
- Schémata s hradly a vodiči
  - Jedná se o vizuální reprezentaci
  - Značení logických hradel se řídí normami – typicky [ANSI nebo IEC](#ansi-iec-norms)
- HDL (Hardware Description Language)
  - Speciální programovací jazyky pro návrh digitálních obvodů na základě popisu jejich struktury či chování
  - Mezi příklady patří VHDL a Verilog

## Multiplexor

- Kombinační logická síť, která **přepíná logické úrovně z více vstupů na jeden výstup**.

- Značí se MX či MUX.

- Pomocí binární kombinace na řídicím vstupu S se vybere logická úroveň z příslušného vstupu D<sub>i</sub> a kopíruje se (generuje se shodná logická úroveň) na výstup Y.

- Řídící vstup S může být vícebitový, obecně platí, že pomocí N-bitového S můžeme vybrat 1 z N<sup>2</sup> datových vstupů D<sub>i</sub>.

<img src="/images/2/1.png" alt="" width="500"/>

Na úrovni hradel, tedy elementárních logických členů, lze tento multiplexor (4-1) sestavit v souladu s výše uvedenou logickou rovnicí za pomoci dvou invertorů, čtyř hradel AND a jednoho hradla OR.:

<img src="/images/2/2.png" alt="" width="300"/>

## Demultiplexor

- Kombinační logická síť s
jedním datovým vstupem D, N adresovými vstupy A<sub>0</sub>..A<sub>N-1</sub> a 2<sup>N</sup> výstupy Y<sub>0</sub>..Y<sub>2<sup>N</sup>-1</sub>.

- Přenáší logickou hodnotu z datového
vstupu D na jeden z 2<sup>N</sup> výstupů, přičemž
ostatní výstupy mají neaktivní logické úrovně (častěji logická 0, ale může být i 1 -- záleží na konkrétním demultiplexoru).

- Funguje tedy jako **přepínač jednoho vstupu na 2<sup>N</sup> výstupů**.

- Značí se DMX 1-2<sup>N</sup>.

<img src="/images/2/3.png" alt="" width="500"/>

Na úrovni hradel vypadá typická realizace demultiplexoru 1-4 následovně:

<img src="/images/2/4.png" alt="" width="250"/>

Tato realizace je v souladu s výše uvedenými logickými rovnicemi pro každý výstup Y<sub>i</sub>.

## Kodér

- Kombinační logická síť, jejímž **výstupem je binární číslo odpovídající pořadí aktivního vstupu**.

- Značí se EN 2<sup>N</sup>-N.

<img src="/images/2/5.png" alt="" width="500"/>

Nelze-li zajistit, aby byl v daném okamžiku aktivní pouze jeden vstup, je
třeba použít **prioritní kodér**.

### Prioritní kodér

- **Výstupem je binární číslo odpovídající pořadí aktivního vstupu s
nejvyšší prioritou, přičemž priorita standardně odpovídá pořadovému číslu**.
Jsou-li např. u prioritního kodéru 8-3 současně aktivní vstupy č. 2, č. 4 a č. 7, na výstupu bude 111 -- binární ekvivalent nejvyššího pořadového čísla (číslo 7) mezi aktivními vstupy.  

- Prioritní kodér disponuje také **výstupem GS**, který **slouží k detekci, zda-li je alespoň jeden vstup aktivní** (logická 1), či nikoliv (logická 0).

<img src="/images/2/6.png" alt="" width="500"/>

## Dekodér

- Kombinační logická síť s N adresovými vstupy a 2<sup>N</sup> výstupy.

- N-bitový **binární kód na vstupu určuje, který výstup bude platný**, přičemž platný je vždy jen jeden.

- **Platný výstup může být aktivní jak v logické 1, tak 0. Ostatní výstupy budou mít opačnou logickou úroveň**.

- Demultiplexor lze využít jako dekodér s tím, že zvolený výstup bude mít logickou úroveň odpovídající úrovni přivedené na datový vstup D. Ta by se neměla měnit.

- Dekodér se označuje jako DC N-2<sup>N</sup>, popř. DC 1-2<sup>N</sup>.

Obrázek níže ukazuje maticovou strukturu dekodéru 4-16 (1 ze 16), jejímž základem je dvojice dekodérů 2-4 (1 ze 4). Tato struktura je vhodná pro velký počet výstupů a běžně se používá u paměťových dekodérů.
 
<img src="/images/2/7.png" alt="" width="500"/>

- "Speciální" typ dekodéru představuje dekodér označovaný jako BCD (Binary Coded Decimal). U tohoto typu dekodéru je na základě prvních deseti binárních čísel aktivován jeden z desíti výstupů. 

---

**POZN.: Propojování kombinačních sítí určených pro menší počty vstupů a výstupů za účelem navýšení počtu vstupů a výstupů je běžná praxe nad rámec dekodérů.**

---

## Binární sčítačka

---

**POZN.: Vzhledem k tomu, že reprezentace čísel a binární aritmetické operace jsou náplní okruhu [Reprezentace čísel a základní dvojkové aritmetické operace v počítači](reprezentace-čísel-a-základní-dvojkové-aritmetické-operace-v-počítači), uvádím zde jen naprosto nezbytné minimum potřebné pro pochopení binárních sčítaček.**

---

### Dvojkový doplněk (Two's complement)

- Představuje základ aritmetiky většiny dnešních číslicových systémů.

- Pomocí N bitů lze reprezentovat 2<sup>(N - 1)</sup> záporných čísel, 2<sup>(N - 1)</sup> - 1 kladných čísel a číslo 0. Např. u 8 bitů je číselný rozsah od -128 do 127.

- Chceme-li dekadické číslo reprezentovat v tomto formátu, musíme se řídit jeho znaménkem. Je-li kladné, jednoduše jej převedeme do binární podoby. Je-li záporné, musíme nejprve převést absolutní hodnotu čísla do binární podoby, následně invertovat všechny bity a nakonec přičíst 1.

- Chceme-li provést opačnou konverzi, tedy z binárního čísla ve dvojkovém doplňku opět získat číslo dekadické, musíme začít kontrolou znaménkového bitu. Tím je MSB (Most Significant Bit), který se běžně nachází nejvíc nalevo. Má-li tento bit hodnotu 1, jde o záporné číslo. Má-li hodnotu 0, jde o kladné číslo. U kladných čísel stačí provést standardní převod binárního čísla na dekadické. U záporných čísel je třeba nejprve invertovat všechny bity a následně přičíst 1.

### Přenos (Carry)

- Sčítáme-li na dané pozici cifry, jejichž součet má vyšší hodnotu, než je základ číselné soustavy, dochází k přenosu do vyššího řádu.

- U sčítání binárních čísel jde o součet dvou jedniček.

### Přetečení (Overflow)

- K přetečení dochází v situaci, kdy se součet dvou binárních čísel dostane mimo rozsah daný dvojkovým doplňkem a daným počtem bitů. 

- K přetečení dochází pouze při sčítání dvou kladných či dvou záporných čísel. Má-li každý ze sčítanců jiné znaménko, přetečení nehrozí.

- Ve většině počítačů se přetečení detekuje příslušným logickým obvodem a uschovává se.

### Poloviční binární sčítačka (Half Adder (HA))

- **Sčítá binární hodnoty z dvojice vstupů (x a y)**

- Postrádá vstup pro přenos z nižšího řádu

- Na výstupu **s** (suma) je hodnota určena jako **x XOR y**

- Na výstupu **c** (carry; přenos do vyššího řádu) pak jako **x AND y**

<img src="/images/2/8.png" alt="" width="300"/>

### Úplná binární sčítačka (Full Adder (FA))

- **Sčítá binární hodnoty z dvojice vstupů x<sub>i</sub> a y<sub>i</sub> a uvažuje** přitom **přenos z nižšího** řádu -- vstup **c<sub>(i - 1)</sub>**.

- Pro výstupy **s<sub>i</sub>** (suma) a **c<sub>i</sub>** (carry; přenos do vyššího řádu) platí následující rovnice:

    **s<sub>i</sub> = x<sub>i</sub> XOR y<sub>i</sub> XOR c<sub>i</sub>**

    **c<sub>i</sub> = (x<sub>i</sub> AND y<sub>i</sub>) OR (x<sub>i</sub> AND c<sub>(i - 1)</sub>) OR (y<sub>i</sub> AND c<sub>(i - 1)</sub>)**

<img src="/images/2/9.png" alt="" width="300"/>

## Vícebitové binární sčítačky

### Sčítačka s přenosem (Ripple Carry Adder (RCA))

Nejjednodušší typ vícebitové binární sčítačky, založený na **propojení úplných sčítaček a propagaci přenosu (carry)**.
**Zpoždění**, které typicky vyjadřujeme jako počet logických členů, **je** zde poměrně **vysoké** kvůli čekání na propagaci přenosu z nižšího řádu.

<img src="/images/2/10.png" alt="" width="500"/>

### Sčítačka s predikcí přenosu (Carry Lookahead Adder (CLA))

<img src="/images/2/11.png" alt="" width="500"/>

Tato sčítačka **je podstatně rychlejší než RCA**, protože **umožňuje paralelní výpočet**.
Místo čekání na propagaci přenosu se **přenos určí předem na základě počátečního přenosu – c<sub>0</sub> a logiky podmínek označovaných jako Generate (G) a Propagate (P)**.
K naplnění podmínky označované **Generate (G)** dojde pokud **součet na dané pozici vygeneruje přenos do vyššího řádu bez ohledu na přenos z nižšího řádu**.
V kontextu binární sčítačky se tak stane, pokud jsou na obou vstupech jedničky.
Podmínku tak lze pro libovolnou pozici vyjádřit následovně.:

**G<sub>i</sub> = x<sub>i</sub> AND y<sub>i</sub>**

K naplnění podmínky **Propagate (P)** dojde, pokud **součet na dané pozici vygeneruje přenos do vyššího řádu pakliže je na tuto pozici propagován přenos z nižšího řádu**.
K tomu postačuje jednička na jednom ze dvou vstupů.
Podmínku lze tedy vyjádřit následovně.:

**P<sub>i</sub> = x<sub>i</sub> OR y<sub>i</sub>**

Na základě vyhodnocení těchto podmínek lze určit hodnoty přenosu.:

**c<sub>1</sub> = G<sub>0</sub> OR (P<sub>0</sub> AND c<sub>0</sub>)**

**c<sub>2</sub> = G<sub>1</sub> OR (P<sub>1</sub> AND c<sub>1</sub>)**

**c<sub>3</sub> = G<sub>2</sub> OR (P<sub>2</sub> AND c<sub>2</sub>)**

**c<sub>4</sub> = G<sub>3</sub> OR (P<sub>3</sub> AND c<sub>3</sub>)**

Provedeme-li substituci, vystačíme si při výpočtu hodnot přenosu s c<sub>0</sub> (dáno předem; při sčítání typicky 0).
Logika potřebná k reprezentaci rovnic po substituci (G<sub>0</sub> OR (P<sub>0</sub> AND c<sub>0</sub>) za c<sub>1</sub> atp.) je ovšem poměrně komplexní a výpočet hodnot přenosu tak nějakou dobu trvá (nejdéle trvá určení c<sub>4</sub> -- rovnice je nejdelší...).
I tak je ale tato sčítačka **výrazně rychlejší než** sčítačka typu **RCA**.
Je ale také náročnější ji vyrobit a je tedy dražší.
Určitý kompromis představuje sčítačka označovaná jako **sčítačka s výběrem přenosu**.

### Sčítačka s výběrem přenosu (Carry Select Adder (CSA))

Tato sčítačka zavádí **paralelismus na úrovni bloků**. Ty jsou typicky čtyřbitové.

<img src="/images/2/12.png" alt="" width="500"/>

**Na úrovni** těchto **bloků je** pomocí dvojice sčítaček typu RCA **vypočítán výsledek a přenos pro obě hodnoty přenosu z předchozího bloku**.
**Platné hodnoty jsou** pak **na základě skutečné hodnoty přenosu z předchozího bloku vybrány pomocí multiplexorů**.
Na obrázku níže je vidět konstrukce 16-bitové sčítačky pomocí jedné 4-bitové sčítačky typu RCA a tří CSA bloků.:

<img src="/images/2/13.png" alt="" width="1000"/>

Sčítačka typu CLA představuje **kompromis mezi RSA a CLA jak z hlediska rychlosti, tak z hlediska náročnosti výroby a tedy i ceny**.
Používá se nejčastěji v případech, kdy nestačí rychlost RSA, ale omezené zdroje neumožní použití CLA.

---

## Doplňující materiály

### FIT

#### Přednáška z předmětu INC

<PdfEmbed fileId="2/1.pdf" />

#### Stream (leSamo)

<YouTubeEmbed id="aUKf7IhOSoE" />

### Další zdroje

<a id="ansi-iec-norms"></a>

#### Google images -- značení logických hradel (normy ANSI a IEC) 

<img src="/images/2/d1.png" alt="" width="500"/>
