---
layout: ../layouts/MDXPage.astro
currentPage: Princip činnosti polovodičových prvků
---

import YouTubeEmbed from './../components/YouTubeEmbed/YouTubeEmbed.tsx'
import PdfEmbed from './../components/PdfEmbed/PdfEmbed.tsx'

# 1.: Princip činnosti polovodičových prvků

- ### Dioda
- ### Bipolární a unipolární tranzistor ve spínacím režimu
- ### Realizace logických členů NAND a NOR v technologii CMOS

## Obecné principy

- Základem polovodičových prvků jsou **křemík (Si)**, **germanium (Ge)** a **uhlík (C)**.
Jedná se o prvky se čtveřicí valenčních elektronů. Jako valenční označujeme elektrony vnější vrstvy obalu atomu, které se účastní chemických vazeb.

- **Elektrony jsou částice s negativním nábojem**.
V elektrickém obvodu tak "tečou" od **-** k **+**.
U elektrického proudu pak značíme, že "teče" od **+** k **-**.
Tato konvence byla zavedena dříve, než byl objeven elektron a jeho role nosiče náboje.

- Elektrická **vodivost polovodičů je na pomezí vodivosti vodičů a izolantů**.
Lze ji ovlivnit pomocí teploty (zvyšuje se s rostoucí teplotou), světla a přidávání příměsí.
Tento proces se nazývá dopování, z angl. dopping.

- Přidáme-li **příměs s větším počtem valenčních elektronů**, např. **fosfor**, který má pět valenčních elektronů, vznikne tzv. **substrát typu N**.

    - Majoritními nositeli náboje jsou zde volné elektrony.
    - Mluvíme o "elektronové vodivosti".

- **Dopujeme-li materiál příměsí s nižším počtem valenčních elektronů**, např. **borem**, který má tři valenční elektrony, vznikne **substrát typu P**.

    - Majoritními nositeli náboje jsou díry.
    - Díry jsou imaginární částice značící chybějící elektrony.
    - Mluvíme o "děrové vodivosti".

- **Oba typy substrátu**, tedy typ P i typ N, **vedou elektrický proud**.

- Propojíme-li tyto typy substrátu, vznikne **PN přechod** a dojde k procesu označovanému jako **rekombinace**, kdy **volné elektrony zaplňují blízké díry** a dochází tak k eliminaci nositelů náboje. Na základě tohoto procesu **vzniká zóna vyčerpání**, z angl. depletion zone, **s nízkou vodivostí** způsobenou nedostatkem nositelů náboje.

- V důsledku přechodu elektronů z N do P se **v P vytváří
záporný a v N kladný náboj**. Tím **vzniká** elektrické pole a **potenciálová bariéra**, představující energetickou překážku, kterou musí nositelé náboje překonat, aby mohli přejít z jedné
strany PN přechodu na druhou. **Pro překonání zóny vyčerpání a potenciálové bariéry je potřeba dodat napětí**. U křemíku se jedná o ~0.7 V.

- Když je substrát typu **P připojen ke kladnému pólu** zdroje napětí a substrát typu **N k zápornému**, PN přechod je polarizován v tzv. **propustném směru**. **Napětí zdroje** v této situaci **snižuje potenciálovou bariéru** a je-li dostatečné (vyšší než je hodnota potenciálové bariéry -- typicky ~0.7 V), **PN přechodem prochází elektrický proud**.

- Je-li **polarizace opačná**, jedná se o tzv. **závěrný směr**, kdy se potenciálová bariéra zvyšuje a zóna vyčerpání zvětšuje, což vede k tomu, že **skrze PN přechod elektrický proud neprochází**.

#### Zapojení PN přechodu (resp. diody) v propustném směru

![](/images/1/1.png)

## Dioda

Dioda je **prvek založený na PN přechodu**, jehož **rolí je propouštět proud pouze v jednom směru**.
Na rozdíl od imaginární "ideální" diody, která skutečně propouští proud pouze v jednom směru,
**reálná dioda propouští proud v jednom směru lépe než v druhém**. V závěrném směru proud prakticky
nepropouští (výjimku představuje velmi malý konstantní proud označovaný jako Reverse Saturation Curent) a
to až do tzv. **průrazného napětí**. **Po dosažení tohoto napětí začne proud strmě narůstat** (jevy Avanalche Breakdown a Tunneling Breakdown),
což obvykle vede ke **zničení diody** (přehřátí a spálení). Výjimku představují **Zenerovy diody** určené
pro práci v závěrném směru, ale i ty lze spálit, pokud přes ně prochází příliš velký proud
(proud lze omezit přidáním odporu (prvek rezistor) viz Ohmův zákon). V propustném směru začne procházející
proud strmě narůstat po překonání potenciálové bariéry (~0.7 V pro křemík).

#### Voltampérová charakteristika (VA-charakteristika) diody

![](/images/1/2.png)

#### Existují různé druhy diod.:

- Standardní (P-N Junction) dioda
  - Standardní typ diody založený na PN přechodu.
  - Umožňuje průchod proudu pouze jedním směrem.
  - Používá se pro usměrňování střídavého proudu na jednosměrný.
- **Zenerova dioda**
  - Navržena pro práci v závěrném směru (3. kvadrant VA charakteristiky diody)
  - Používá se pro regulaci napětí a jako referenční element (poskytuje referenční napětí)
- Schottkyho dioda
  - Má nižší hodnotu potenciálové bariéry (0.15 - 0.4 V) a reaguje rychleji než ostatní diody
  - Využívá se ve vysokofrekvenčních aplikacích, napájecích obvodech a usměrňovačích
- LED (Light Emitting Diode)
  - Tato dioda vyzařuje světlo když skrze ni prochází proud
  - Používá se jako indikátor a také v displejích
- Fotodioda
  - Je citlivá na světlo
  - Může pracovat ve dvou režimech
    - Generátor elektrického proudu úměrného intenzitě světla
    - Spínač reagující na světlo
  - Používá se v optických detektorech a solárních panelech
- Varikap (Varaktorová dioda)
  - Funguje jako kondenzátor, jehož kapacita se mění s napětím
  - Používá se pro ladění a filtraci frekvence

### Jednocestný usměrňovač

Principem jednocestného usměrňovače, který je běžnou aplikací standardní diody, je to, že
**propouští pouze jednu polovinu vlny střídavého proudu**. **Ze střídavého proudu** tak **vznikne pulzující stejnosměrný
proud**. Je také potřeba počítat s úbytkem napětí na diodě (úbytek ve výši potenciálové bariéry -- typicky ~0.7 V).
Transformaci proudu pomocí jednocestného usměrňovače ukazuje obrázek níže.:

<img src="/images/1/2b.png" alt="" width="600"/>

Samotný jednocestný usměrňovač včetně jednofázového zdroje střídavého napětí a zátěže (rezistor) pak následující animace.:

<img src="/images/1/g1.gif" alt="" width="400"/>

### Dvoucestný usměrňovač

Dokonalejší alternativou k jednocestnému usměrňovači je dvoucestný usměrňovač.
Ten umožňuje efektivní využití obou polovin vlny střídavého proudu.
Existují dvě základní konfigurace.:

- Usměrňovač se středovým vývodem (s půleným vinutím)
  - Vyžaduje transformátor se středovým vývodem
  - Je zkonstruován pomocí dvojice diod
- Můstkový usměrňovač
  - Je zkonstruován pomocí čtyř diod
  - Jedná se o nejběžnější a nejefektivnější konfiguraci pro dvoucestné usměrnění

Výstupem dvoucestného usměrňovače pulzující stejnosměrný proud, jehož frekvence je dvakrát vyšší
než u jednocestného usměrňovače. To umožňuje snazší vyhlazení proudu, čehož lze dosáhnout např. pomocí kondenzátorů.
Princip dvoucestného můstkového usměrňovače zobrazuje následující animace.:

<img src="/images/1/g2.gif" alt="" width="400"/>

## Tranzistor

Jedná se o součástku, která **má tři** základní **komponenty**: **kolektor, emitor a báze** (či základna). V obvodech funguje buďto jako **zesilovač**, či jako **velice rychlý, proudem kontrolovaný spínač**. Teče-li do báze proud, poteče též mezi kolektorem a emitorem.
Jak ale zobrazuje obrázek níže, proud teče "obráceně" než se pohybují samotné elektrony a zásadní je právě pohyb elektronů. 

<img src="/images/1/3.png" alt="" width="200"/>

### Bipolární tranzistor BJT (Bipolar Junction Transistor)

- Tento typ tranzistoru tvoří **dva PN přechody** -- buďto PNP nebo **NPN**.
- **Přenos proudu zajišťují oba nosiče** (elektrony i díry)

#### Princip NPN

Emitor je připojen na katodu (záporný pól; elektrodu uvolňující elektrony) napájecího zdroje. Společně s bází pak tvoří "diodu" zapojenou v propustném směru. Pokud je na bázi přivedeno napětí o ~0.7 V vyšší (standardní hodnota potenciálové bariéry viz výše) než na emitor, začne touto "diodou" procházet proud. Díky tomu, že emitor bývá již z výroby podstatně více dopován než báze, dostává se do báze podstatně více volných elektronů, než je zde děr. Báze je navíc tenká, takže elektrony snadno přitáhne kladný náboj vznikající na kolektoru, který je připojen na anodu (kladný pól) primárního napájecího zdroje. Tranzistorem tak prochází proud (z kolektoru do emitoru; elektrony se pohybují obráceně). Toto se týká cca 99% elektronů z emitoru, přibližně 1% "dá přednost" anodě sekundárního zdroje připojené k bázi.

<img src="/images/1/4.png" alt="" width="500"/>

#### Princip PNP

Emitor je připojen na anodu (kladný pól), kolektor na katodu (kladný pól) a napětí na emitoru musí být o ~0.7 V vyšší než napětí na bázi. Proud pak "teče" z emitoru do kolektoru, tedy obráceně než u NPN tranzistorů. Mechanismus u obou druhů bipolárních tranzistorů nám přijde podobnější, uvážíme-li u NPN pohyb elektronů a u PNP pohyb děr. 

<img src="/images/1/5.png" alt="" width="800"/>

##### IEL flashback

Chceme-li "otevřít" resp. zprůchodnit tranzistor NPN, přivedeme na bázi logickou 1, tedy vyšší úroveň napětí, než je na emitoru. Chceme-li téhož dosáhnout u PNP tranzistoru, přivedeme na bázi logickou 0, tedy nížší úroveň napětí, než je na emitoru, popř. zem.

### Unipolární tranzistor MOSFET (Metal Oxide Semiconductor Field Effect Transistor)

- **Přenos proudu zajišťuje pouze jeden z nosičů** (elektrony či díry) -- **N-channel** nebo **P-channel**

- Tranzistory tohoto typu se dále dělí na kategorie:
  1. **Enhancement: nevede proud, musí se "zapnout"**
  2. **Depletion: vede proud, musí se "vypnout"**

- Tranzistor je řízen napětím.

- Čtyři elektrody:
    1. Gate
    2. Source
    3. Drain
    4. Body (Substrate)

<img src="/images/1/6.png" alt="" width="500"/>

#### Princip

Na source a na body je přivedeno záporné napětí či zem. Tím se zamezí tomu, aby PN přechod fungoval klasickým způsobem. Na drain je následně přivedeno kladné napětí. Tranzistor je symetrický, takže source a drain lze zaměnit. Na gate je následně přivedeno kladné napětí, což vede k tomu, že kolem této elektrody vznikne kladný náboj, který začne přitahovat elektrony z body a ze source, zatím co díry se přesouvají dolů směrem k body. Důležitou roli hraje vrstva izolace, díky které elektrony nemohou přes gate P substrát opustit. Místo toho se tak začne vytvářet **kanál**, který **při dostatečném napětí** přivedeném **na gate** umožní **průchod proudu z drain do source** (elektrony "tečou" ze source do drain). **Na určité úrovni napětí** primárního zdroje **se procházející proud "zasekne"** (Saturation current). Chceme-li, aby tranzistorem procházel větší proud, musíme zvýšit napětí na gate. Tuto závislost ilustrují obrázky níže.

<img src="/images/1/7.png" alt="" width="500"/>

V<sub>GS</sub> je napětí přivedené gate (resp. mezi gate a source), zatím co V<sub>DS</sub> je napětí primárního zdroje (resp. mezi drain a source).

<img src="/images/1/8.png" alt="" width="500"/>

## CMOS (Complementary metal–oxide–semiconductor)

- Technologie výroby logických členů pomocí párů **P-channel (PMOS)** a **N-channel (NMOS)** **MOSFET** tranzistorů.

- Mezi její přednosti patří nízká spotřeba, odolnost proti šumu a menší produkce tepla.

### PMOS

- Tranzistor sepne **záporné** napětí (**logická 0**) na gate.
- Nositelem náboje jsou **díry**
- Source a body se propojují s **anodou** (kladným pólem zdroje napětí)
- Drain se propojuje s **anodou** (kladným pólem zdroje napětí) (záporným pólem zdroje napětí)
- Source a drain jsou typu **P** zatím co body je typu **N**
- Na source je **vyšší** napětí než na drain

### NMOS

- Tranzistor sepne **kladné** napětí (**logická 1**) na gate.
- Nositelem náboje jsou **elektrony**
- Source a body se propojují s **katodou** (záporným pólem zdroje napětí)
- Drain se propojuje s **anodou** (kladným pólem zdroje napětí)
- Source a drain jsou typu **N** zatím co body je typu **P**
- Na source je **nižší** napětí než na drain

<img src="/images/1/9.png" alt="" width="800"/>

### Princip výroby

- Celý **čip se nadopuje na typ P**
- Tranzistory typu **NMOS lze vytvořit přímo** přidáním N kanálů
- **Vytvoření tranzistorů typu PMOS předchází vytvoření tzv. studny (well) nadopováním oblasti čipu na typ N**, přičemž do této oblasti jsou následně vloženy P kanály
- Výhodnost tohoto principu výroby je postavena to tom, že **je** obvykle **potřeba více NMOS tranzistorů**, např. jedna buňka paměti typu SRAM (Static Random Access Memory) obsahuje 4 NMOS tranzistory a 2 PMOS tranzistory.

<img src="/images/1/10.png" alt="" width="800"/>

### Logické členy NAND a NOR

#### NAND (Shefferova funkce)

<img src="/images/1/11.png" alt="" width="400"/>

#### NOR (Peirceova funkce)

<img src="/images/1/12.png" alt="" width="400"/>

Máme-li nakreslit hradla na bázi CMOS (resp. PMOS či MNOS) tranzistorů, je důležité vycházet z pravdivostní
tabulky dané funkce (viz výše) a z faktu, že PMOS tranzistor zprůchodní logická 0 na přivedená na gate, zatím co NMOS tranzistor logická 1.
Logickou 0 (zem) i 1 (zdroj napětí (Vdd)) máme k dispozici a je potřeba akorát správně "namapovat" vstup na výstup v souladu s již zmiňovanou pravdivostní tabulkou.
CMOS hradla pro funkce NAND a NOR zobrazuje obrázek níže. 

- A a B jsou vstupy, C výstup.
- Nahoře je logická 1 (Vdd), dole logická 0 (zem).

<img src="/images/1/13.png" alt="" width="600"/>

---

## Doplňující materiály

### FIT

#### PePeho přednáška z předmětu IEL

<PdfEmbed fileId="1/1.pdf" />

#### Stream (leSamo)

<YouTubeEmbed id="JXxxdRiKAlk" />

### Další zdroje

#### YouTube -- Polovodiče a PN přechod

<YouTubeEmbed id="33vbFFFn04k" />

#### YouTube -- Diody

<YouTubeEmbed id="-SSkjWuUri4" />

#### YouTube -- Bipolární tranzistory (NPN)

<YouTubeEmbed id="DXvAlwMAxiA" />

#### YouTube -- MOSFET

<YouTubeEmbed id="rkbjHNEKcRw" />
